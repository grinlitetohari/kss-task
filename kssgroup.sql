-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 23, 2021 at 09:46 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kssgroup`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id_employee` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `birthday` date NOT NULL,
  `job_field` varchar(100) NOT NULL,
  `job_position` varchar(100) NOT NULL,
  `is_deleted` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id_employee`, `first_name`, `last_name`, `email`, `gender`, `birthday`, `job_field`, `job_position`, `is_deleted`) VALUES
('emp_20210723_fHb0Fz74Xpiuer62I5DJ', 'Andrea', 'Sina Aliee', 'sina@mail.com', 'Female', '1990-09-07', 'Logistics', 'Manager', 'no'),
('emp_20210723_INCXKiSmVwetMAxWTDzf', 'Karim', 'Maulana', 'karim@mail.com', 'Male', '1995-02-17', 'Marketing', 'Team Leader', 'no'),
('emp_20210723_mqKO1pNodUcJVYjH6gf2', 'Raul', 'Fernandez', 'raul@mail.com', 'Male', '1985-01-22', 'Legal & Security', 'Staff', 'no'),
('emp_20210723_PC6HFZEceVvTJGkxhuW2', 'Imam', 'Tohari', 'imamtohari@mail.com', 'Male', '1989-03-02', 'Finance', 'Staff', 'no'),
('emp_20210723_ponHhVxSiTlAI4m2yKze', 'Harry', 'Bee', 'bee@mail.co.id', 'Male', '1992-09-17', 'Information Technology', 'Manager', 'no'),
('emp_20210723_psDCEqYuLHWzQO3F9crB', 'Brian ', 'Guss Free', 'bk@mail.com', 'Male', '1987-09-01', 'Human Resource', 'Staff', 'no'),
('emp_20210723_wWrl01cVHqRzPbuK8hfg', 'Sarah', 'Diva Aliee', 'sda@mail.com', 'Female', '1991-05-01', 'Information Technology', 'Staff', 'no');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
