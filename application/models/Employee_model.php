<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

	public function get_employee()
	{
		$this->db->select('id_employee, first_name, last_name, gender, email, job_field, job_position');
		$this->db->where('is_deleted', 'no');
		$this->db->order_by('job_field', 'ASC');
		$this->db->order_by('job_position', 'ASC');
		$query = $this->db->get('employee');
		return $query->result();
	}
	public function get_employee_detail($where = NULL)
	{
		$this->db->select('id_employee, first_name, last_name, birthday, gender, email, job_field, job_position');
		$this->db->where('is_deleted', 'no');
		$this->db->where($where);
		$query = $this->db->get('employee');
		return $query->row();
	}
	public function insert_employee()
	{
		$employee = array(
			'id_employee' => $this->_get_id(),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'gender' => $this->input->post('gender'),
			'birthday' => $this->input->post('birthday'),
			'job_field' => $this->input->post('job_field'),
			'job_position' => $this->input->post('job_position'),
			'is_deleted' => 'no'
		);
		return $this->db->insert('employee', $employee);
	}
	public function update_employee()
	{
		$where = array(
			'id_employee' => $this->input->post('id_employee'),
			'is_deleted' => 'no'
		);
		$employee = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'gender' => $this->input->post('gender'),
			'birthday' => $this->input->post('birthday'),
			'job_field' => $this->input->post('job_field'),
			'job_position' => $this->input->post('job_position')
		);
		return $this->db->update('employee', $employee, $where);
	}
	public function delete_employee($where = NULL)
	{
		if($where != NULL)
		{
			$employee = array(
				'is_deleted' => 'yes'
			);
			return $this->db->update('employee', $employee, $where);
		}
		else
		{
			return FALSE;
		}
	}
	private function _get_id()
	{
		$id = 'emp_'.date('Ymd').'_'.random_string('alnum', 20);
		return $id;
	}
}
