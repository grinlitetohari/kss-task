<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	private $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('employee_model', 'em');
	}
	public function index()
	{
		$this->data['employee'] = $this->em->get_employee();
		$this->data['content'] = 'employee/read';
		$this->load->view('template', $this->data);
	}

	public function create()
	{
		$this->_validation_setting();
		if($this->form_validation->run() == FALSE)
		{
			$this->data['job_field'] = $this->_get_job_field();
			$this->data['job_position'] = $this->_get_job_position();
			$this->data['content'] = 'employee/create';
			$this->load->view('template', $this->data);
		}
		else
		{
			if($this->em->insert_employee())
			{
				$toast_alert_data = array(
					'message' => 'Add employee data success',
					'type' => 'success'
				);
			}
			else
			{
				$toast_alert_data = array(
					'message' => 'Add employee data failed',
					'type' => 'danger'
				);
			}
			$this->_redirect($toast_alert_data);
		}
	}

	public function detail($id_employee = NULL)
	{
		$where = array('id_employee' => $id_employee);
		$this->data['employee'] = $this->em->get_employee_detail($where);
		if( $this->data['employee']!= NULL)
		{
			$this->data['content'] = 'employee/detail';
			$this->load->view('template', $this->data);
		}
		else
		{
			$this->_data_not_found_alert('Get employee detail failed. Data not found');
		}
	}
	public function update($id_employee = NULL)
	{
		$where = array('id_employee' => $id_employee);
		$this->data['employee'] = $this->em->get_employee_detail($where);
		if( $this->data['employee']!= NULL)
		{
			$this->_validation_setting();
			if($this->form_validation->run() == FALSE)
			{
				$this->data['gender'] = $this->_get_gender();
				$this->data['job_field'] = $this->_get_job_field();
				$this->data['job_position'] = $this->_get_job_position();
				$this->data['content'] = 'employee/update';
				$this->load->view('template', $this->data);
			}
			else
			{
				if($this->em->update_employee())
				{
					$toast_alert_data = array(
						'message' => 'Update employee data success',
						'type' => 'success'
					);
				}
				else
				{
					$toast_alert_data = array(
						'message' => 'Update employee data failed',
						'type' => 'danger'
					);
				}
				$this->_redirect($toast_alert_data);
			}
		}
		else
		{
			$this->_data_not_found_alert('Update employee data failed. Data not found');
		}
	}

	public function delete($id_employee = NULL)
	{
		$where = array(
			'id_employee' => $id_employee,
			'is_deleted' => 'no'
		);
		$this->data['employee'] = $this->em->get_employee_detail($where);
		if( $this->data['employee']!= NULL)
		{
			if($this->em->delete_employee($where))
			{
				$toast_alert_data = array(
					'message' => 'Delete employee data success',
					'type' => 'success'
				);
			}
			else
			{
				$toast_alert_data = array(
					'message' => 'Delete employee data failed',
					'type' => 'danger'
				);
			}
			$this->_redirect($toast_alert_data);
		}
		else
		{
			$this->_data_not_found_alert('Delete employee data failed. Data not found');
		}
	}
	public function check_birthday_format()
	{
		$birthday = $this->input->post('birthday');
		if(trim($birthday) != '')
		{
			if(preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $birthday))
			{
				return TRUE;
			}
			else
			{
				$this->form_validation->set_message('check_birthday_format', 'The {field} field is not in the correct format.');
				return FALSE;
			}
		}
		else
		{
			$this->form_validation->set_message('check_birthday_format', 'The {field} field is required.');
			return FALSE;
		}
	}
	private function _validation_setting()
	{
		$this->form_validation->set_error_delimiters('<div class="form-text text-danger">', '</div>');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('birthday', 'Birthday', 'callback_check_birthday_format');
		$this->form_validation->set_rules('job_field', 'Job Field', 'required');
		$this->form_validation->set_rules('job_position', 'Job Position', 'required');
	}
	private function _get_gender()
	{
		$gender = array(
			'Male',
			'Female'
		);
		return $gender;
	}
	private function _get_job_field()
	{
		$job_field = array(
			'Human Resource',
			'Marketing',
			'Finance',
			'Logistics',
			'Legal & Security',
			'Information Technology'
		);
		return $job_field;
	}
	private function _get_job_position()
	{
		$job_position = array(
			'Team Leader',
			'Manager',
			'Assistant Manager',
			'Director',
			'Staff',
		);
		return $job_position;
	}
	private function _redirect($toast_alert_data = array('message' => 'Message not specify. Call administrator', 'type' => 'danger'))
	{
		$this->session->set_flashdata('toast_alert', $toast_alert_data);
		redirect(site_url('employee'));
	}
	private function _data_not_found_alert($message = 'Data not found')
	{
		$toast_alert_data = array(
			'message' => $message,
			'type' => 'danger'
		);
		$this->_redirect($toast_alert_data);
	}
}
