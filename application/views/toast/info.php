<?php
	$toast = $this->session->flashdata('toast_alert');
?>
<div class="toast-container position-absolute top-0 start-50 translate-middle-x p-3" id="toastPlacement">
	<div class="toast toast-alert align-items-center text-white bg-<?php echo $toast['type']; ?> border-0" role="alert" aria-live="assertive" aria-atomic="true">
		<div class="d-flex">
	    	<div class="toast-body">
	      		<?php echo $toast['message']; ?>
	    	</div>
	  	</div>
	</div>
</div>
