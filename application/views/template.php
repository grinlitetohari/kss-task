<!DOCTYPE html>
<html lang="en">
  	<head>
    	<meta charset="utf-8" />
    	<title>KSS Group</title>
    	<meta name="description" content="KSS Group" />
    	<meta name="author" content="Imam Tohari" />
    	<meta name="viewport" content="width=device-width, initial-scale=1" />
    	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
  	</head>

  	<body>
	  	<div class="container">
		  	<div class="row row-cols-auto align-items-center g-1 mt-4 row-header">
			  	<div class="col">
				  	<img src="<?php echo base_url('assets/images/kss_group_logo.jpg'); ?>" width="64px" />
			  	</div>
			  	<div class="col">
				  	<h4 class="text-secondary"><b>KSS</b> Group</h4>
		  	  	</div>
		  	</div>
		  	<div class="row row-content mt-2">
			  	<?php
					if(isset($content))
					{
						$this->load->view($content);
					}
				?>
		  	</div>
	  	</div>

    <!-- Your content goes here -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script>
		var toastElList = [].slice.call(document.querySelectorAll('.toast-alert'))
		var toastList = toastElList.map(function (toastEl) {
			return new bootstrap.Toast(toastEl);
		})
		toastList.forEach(toast => toast.show());

		function delete_confirm(link, first_name)
		{
			var toastElList = [].slice.call(document.querySelectorAll('.toast-delete-confirm'))
			var toastList = toastElList.map(function (toastEl) {
				return new bootstrap.Toast(toastEl);
			})

			document.getElementById("toast-message").innerHTML = 'Are you sure you want to delete employee '+first_name+' data?';
			document.getElementById("toast-link").href = link;


			toastList.forEach(toast => toast.show());
		}
	</script>
  	</body>
</html>
