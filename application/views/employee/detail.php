<div class="col-12 my-3">
	<h4 class="text-secondary mb-3">Employee Detail</h4>
	<dl class="row">
  		<dt class="col-sm-3">First Name</dt>
  		<dd class="col-sm-9"><?php echo $employee->first_name; ?></dd>
		<dt class="col-sm-3">Last Name</dt>
  		<dd class="col-sm-9"><?php echo $employee->last_name; ?></dd>
		<dt class="col-sm-3">Email</dt>
  		<dd class="col-sm-9"><?php echo $employee->email; ?></dd>
		<dt class="col-sm-3">Gender</dt>
  		<dd class="col-sm-9"><?php echo $employee->gender; ?></dd>
		<dt class="col-sm-3">Birthday</dt>
  		<dd class="col-sm-9"><?php echo $employee->birthday; ?></dd>
		<dt class="col-sm-3">Job Field</dt>
  		<dd class="col-sm-9"><?php echo $employee->job_field; ?></dd>
		<dt class="col-sm-3">Job Position</dt>
  		<dd class="col-sm-9"><?php echo $employee->job_position; ?></dd>
	</dl>
	<a class="btn btn-secondary" href="<?php echo site_url('employee'); ?>"><i class="bi-chevron-left me-2"></i> Back to Employee List</a>
	<a class="btn btn-primary" href="<?php echo site_url('employee/update/'.$employee->id_employee); ?>">Update Employee Data <i class="bi-chevron-right ms-2"></i></a>
</div>
