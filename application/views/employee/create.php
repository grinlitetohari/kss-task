<div class="col-12 my-3">
	<h4 class="text-secondary mb-3">Add Employee</h4>
	<form method="post" action="<?php echo site_url('employee/create'); ?>" class="row g-3">
	  	<div class="col-md-12">
	    	<label for="first_name" class="form-label">First Name</label>
	    	<input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>">
			<?php echo form_error('first_name'); ?>
	  	</div>
	  	<div class="col-md-12">
	    	<label for="last_name" class="form-label">Last Name</label>
	    	<input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>">
			<?php echo form_error('last_name'); ?>
	  	</div>
		<div class="col-12">
	    	<label for="email" class="form-label">Email</label>
	    	<input type="text" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>">
			<?php echo form_error('email'); ?>
	  	</div>
		<div class="col-12">
	    	<label for="gender" class="form-label">Gender</label>
			<div class="form-check">
			  	<input class="form-check-input" type="radio" name="gender" id="gender-male" value="male" checked>
			  	<label class="form-check-label" for="gender-male">Male</label>
			</div>
			<div class="form-check">
			  	<input class="form-check-input" type="radio" name="gender" id="gender-female" value="female">
			  	<label class="form-check-label" for="gender-female">Female</label>
			</div>
			<?php echo form_error('gender'); ?>
	  	</div>
		<div class="col-12">
	    	<label for="birthday" class="form-label">Birthday (Example : 1990-12-25)</label>
	    	<input type="text" class="form-control" name="birthday" id="birthday" value="<?php echo set_value('birthday'); ?>">
			<?php echo form_error('birthday'); ?>
	  	</div>
		<div class="col-12">
    		<label for="job_field" class="form-label">Job Field</label>
    		<select id="job_field" name="job_field" class="form-select">
      			<?php
					foreach($job_field as $row)
					{
						echo '<option value="'.$row.'" '.set_select('job_field', $row).'>'.$row.'</option>';
					}
				?>
    		</select>
			<?php echo form_error('job_field'); ?>
  		</div>
		<div class="col-12">
    		<label for="job_position" class="form-label">Job Position</label>
    		<select id="job_position" name="job_position" class="form-select">
      			<?php
					foreach($job_position as $row)
					{
						echo '<option value="'.$row.'" '.set_select('job_position', $row).'>'.$row.'</option>';
					}
				?>
    		</select>
			<?php echo form_error('job_position'); ?>
  		</div>
		<div class="col-12">
			<a class="btn btn-secondary" href="<?php echo site_url('employee'); ?>"><i class="bi-chevron-left me-2"></i> Back to Employee List</a>
    		<button type="submit" class="btn btn-primary"><i class="bi-save me-2"></i> Save Employee Data</button>
  		</div>
	</form>
</div>
