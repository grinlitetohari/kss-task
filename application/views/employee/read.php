<div class="col-12 mt-3">
	<a class="btn btn-primary my-4" href="<?php echo site_url('employee/create'); ?>"><i class="bi-plus-lg me-2"></i> Add Employee Data</a>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Gender</th>
				<th>Email</th>
				<th>Job Detail</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php
				if($employee != NULL)
				{
					$counter = 0;
					foreach($employee as $row)
					{
						$counter++;
						$fullname = $row->first_name.' '.$row->last_name;
						$delete_link = site_url('employee/delete/'.$row->id_employee);
						echo '<tr>
							<td>'.$counter.'</td>
							<td>'.$fullname.'</td>
							<td>'.ucfirst($row->gender).'</td>
							<td>'.$row->email.'</td>
							<td>'.$row->job_field.' - '.$row->job_position.'</td>
							<td>
								<a class="text-secondary" href="'.site_url('employee/detail/'.$row->id_employee).'" title="Detail"><i class="bi-eye"></i></a>
								<a class="text-secondary ms-4" href="'.site_url('employee/update/'.$row->id_employee).'" title="Edit"><i class="bi-pencil-square"></i></a>
								<a class="text-secondary ms-4" href="#" onclick="delete_confirm(\''.$delete_link.'\', \''.$fullname.'\')" title="Delete"><i class="bi-trash"></i></a>
							</td>
						</tr>';
					}
				}
				else
				{
					echo '<tr><td colspan="6">Employee data is empty</td></tr>';
				}
			?>
		</tbody>
	</table>
</div>
<?php
	if($this->session->flashdata('toast_alert'))
	{
		$this->load->view('toast/info');
	}
?>
<div class="toast-container position-absolute top-0 start-50 translate-middle-x p-3" id="toastPlacement">
	<div class="toast toast-delete-confirm" role="alert" aria-live="assertive" aria-atomic="true" data-bs-autohide="false">
	  	<div class="toast-body">
	    	<div id="toast-message"></div>
	    	<div class="mt-2 pt-2 border-top">
				<button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="toast">Cancel</button>
	      		<a id="toast-link" class="btn btn-primary btn-sm" href="#">Delete this data</a>
	    	</div>
	  	</div>
	</div>
</div>
